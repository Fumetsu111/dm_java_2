import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{
    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }
    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
      if(liste.isEmpty())
          return null;
    	Integer res=liste.get(0);
    	for (int elem : liste) {
    		if (elem<res) {
    			res=elem;
        }
    	}
    	return res;
    }
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
    	for (T elem : liste) {
    		if (valeur.compareTo(elem)>-1)
    			return false;
    	}
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
    	ArrayList res=new ArrayList();
    	for (T elem : liste2) {
    		if (liste1.contains(elem) && !res.contains(elem)) {
    			res.add(elem);
    		}
    	}
        return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
     public static List<String> decoupe(String texte){
         ArrayList<String> res = new ArrayList<String>();
         if(texte.length()!=0){
             for(String elem:texte.split(" ")){
                 if(elem.length()!=0){
                     res.add(elem);
                 }
             }
         }
         return res;
     }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){

      if(texte.length()==0)
          return null;

    	List<String> liste= decoupe(texte);
      Collections.sort(liste);
      List<String> lesMots = new ArrayList<String>();
      List<Integer> nbMots = new ArrayList<Integer>();


      for(String mot:liste){
          if(!lesMots.contains(mot)){
              lesMots.add(mot);
              nbMots.add(1);
          }else{
              nbMots.set(lesMots.indexOf(mot), nbMots.get(lesMots.indexOf(mot)) + 1);
          }
      }
      Integer max = nbMots.get(0);
      for(Integer nb:nbMots){
          if(nb>max)
              max = nb;
      }
      return lesMots.get(nbMots.indexOf(max));
  }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
      int nb = 0;
      if(chaine.length()%2 == 1)
          return false;
      for(int i=0; i<chaine.length(); i++){
          if(nb<0)
              return false;
          if(chaine.charAt(i)=='(')
              nb++;
          if(chaine.charAt(i)==')')
              nb--;
      }
        return true;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
      int crochet=0;
      int parenthese=0;
      int parentheseF=0;
      int parentheseO=0;
      int crochetF=0;
      int crochetO=0;

      boolean res=true;
      char prochainfaux='a';
      for (int i = 0;i<chaine.length();i++){

          if (crochet<0 || parenthese<0)
              res=false;

          if (chaine.charAt(i)==prochainfaux)
              res=false;

          if(chaine.charAt(i)=='['){
              crochet+=1;
              prochainfaux=')';
              crochetO+=1;
          }
          if(chaine.charAt(i)=='('){
              parenthese+=1;
              prochainfaux=']';
              parentheseO+=1;
          }

          if(chaine.charAt(i)==')'){
              parenthese-=1;
              prochainfaux='a';
              parentheseF+=1;
          }

          if(chaine.charAt(i)==']'){
              crochet-=1;
              prochainfaux='a';
              crochetF+=1;
          }
      }

      if(parentheseO != parentheseF || crochetF != crochetO)
          res=false;
      if(parenthese%2 == 1 || crochet%2 == 1)
          res=false;
      return res;

  }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      if(liste.isEmpty())
          return false;
      int debut=0;
    	int fin=liste.size()-1;
    	int milieu=(debut+fin)/2;
    	while(debut<=fin) {
    		if(liste.get(milieu)==valeur) {
    			return true;
    		}
    		else if(liste.get(milieu)>valeur) {
    			fin=milieu-1;
    		}
    		else {
    			debut=milieu+1;
    		}
    		milieu=(debut+fin)/2;
    	}
        return false;
    }



}
